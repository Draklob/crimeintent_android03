package com.programming.draklob.criminalintent;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Draklob on 28/01/2018.
 */

public class Crime {

    // UUID is a Java utility to provide an easy way to generate universally unique ID values.
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private boolean mSolved;

    public Crime() {
        mId = UUID.randomUUID();
        mDate = new Date();     // Using default constructor to set the default current date.
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public void setSolved(boolean solved) {
        mSolved = solved;
    }
}
